package com.example.tysmaxmvvm.Room;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.example.tysmaxmvvm.Model.TmdbTopRatedMovies;

import java.util.List;

@Dao
public interface TopRatedMovieDio {


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void inserttopratedmovie (List<TmdbTopRatedMovies> list);

    @Query("SELECT * FROM TopRatedMovieDB")
    public LiveData<List<TmdbTopRatedMovies>> getallTopratedmovie();


    @Query("DELETE FROM TopRatedMovieDB")
    public void allremovetopratedmovie();
}
