package com.example.tysmaxmvvm.Room;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.tysmaxmvvm.Model.TmdbTopRatedMovies;

@androidx.room.Database(entities = {TmdbTopRatedMovies.class}, version = 1)
public abstract class Database extends RoomDatabase {

    public abstract TopRatedMovieDio topRatedMovieDio();
    public static volatile Database database;


    public static Database getDatabase(final Context context){
        if(database == null){
            synchronized (Database.class){

                    database = Room.databaseBuilder(context.getApplicationContext(),
                            Database.class, "database")
                            .fallbackToDestructiveMigration()
                            .addCallback(callback)
                            .build();

            }
        }
        return database;
    }

    static Callback callback = new Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new RemoveCallBack(database).execute();
        }
    };


    static class RemoveCallBack extends AsyncTask<Void, Void, Void>{

        private TopRatedMovieDio dio;
        public RemoveCallBack(Database database){
            dio = database.topRatedMovieDio();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            dio.allremovetopratedmovie();
            return null;
        }
    }

}
