package com.example.tysmaxmvvm.Adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.VideHolder.RecommendationViewHolder;
import com.example.tysmaxmvvm.Data.DataManager;
import com.example.tysmaxmvvm.Model.Movie;
import com.example.tysmaxmvvm.Model.Tmdb_Movie;
import com.example.tysmaxmvvm.R;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.text.DecimalFormat;
import java.util.List;

public class RecommendationAdapter extends RecyclerView.Adapter<RecommendationViewHolder> {

    private List<Tmdb_Movie> movieList;
    private SetOnClick SetOnClick;

    public void setMovieList(List<Tmdb_Movie> movieList) {
        this.movieList = movieList;
    }

    @NonNull
    @NotNull
    @Override
    public RecommendationViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recommendation_layout, parent, false);
        return new RecommendationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull RecommendationViewHolder holder, int position) {
        holder.Title.setText(movieList.get(position).getTitle());
        holder.Popularty.setText(String.valueOf(movieList.get(position).getVote_count())+" Popularity");
        Picasso.get().load(DataManager.Imagekey+movieList.get(position).getPoster_path()).into(holder.ImageView);
        double val = Double.valueOf(movieList.get(position).getVote_average());
        holder.Rating.setText(String.valueOf(new DecimalFormat("#.#").format(val)));


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetOnClick.OnClick(holder.getAdapterPosition(), movieList.get(position).getId(), movieList.get(position).getOriginal_language(),
                        movieList.get(position).getOverview(), movieList.get(position).getPopularity(), movieList.get(position).getPoster_path(), movieList.get(position).getBackdrop_path(), movieList.get(position).getRelease_date(),
                        movieList.get(position).getTitle(), movieList.get(position).getVote_average(), movieList.get(position).getVote_count());
            }
        });
    }

    @Override
    public int getItemCount() {
        if(movieList == null){
            return 0;
        }else {
            return movieList.size();
        }
    }

    public interface SetOnClick{

        void OnClick(int adapterPosition, int id, String original_language, String overview, String popularity, String poster_path, String backdrop_path, String release_date, String title, String vote_average, int vote_count);
    }

    public void SetOnclickLisiner(SetOnClick SetOnClick){
        this.SetOnClick = SetOnClick;
    }
}
