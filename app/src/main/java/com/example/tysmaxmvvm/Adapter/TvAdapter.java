package com.example.tysmaxmvvm.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.VideHolder.TvViewHolder;
import com.example.tysmaxmvvm.Data.DataManager;
import com.example.tysmaxmvvm.Model.TvModel;
import com.example.tysmaxmvvm.R;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.text.DecimalFormat;
import java.util.List;

public class TvAdapter extends RecyclerView.Adapter<TvViewHolder> {

    private List<TvModel> tvModelList;

    public void setTvModelList(List<TvModel> tvModelList) {
        this.tvModelList = tvModelList;
    }
    private OnClick OnClick;

    @NonNull
    @NotNull
    @Override
    public TvViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tvshow_singlelayout, parent, false);
        return new TvViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull TvViewHolder holder, int position) {
        Picasso.get().load(DataManager.Imagekey+tvModelList.get(position).getPoster_path())
                .into(holder.imageView);

        holder.Title.setText(tvModelList.get(position).getName());
        double val = Double.valueOf(tvModelList.get(position).getVote_average());
        holder.Rating.setText(String.valueOf(new DecimalFormat("#.#").format(val)));
        holder.Popularty.setText(tvModelList.get(position).getPopularity()+" Popularity");

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnClick.Click(tvModelList.get(position).get_id());
            }
        });
    }

    @Override
    public int getItemCount() {
        if(tvModelList == null){
            return 0;
        }else {
            return tvModelList.size();
        }
    }

    public interface OnClick{
        void Click(int TvID);
    }

    public void SetOnclicLisiner(OnClick OnClick){
        this.OnClick = OnClick;
    }
}
