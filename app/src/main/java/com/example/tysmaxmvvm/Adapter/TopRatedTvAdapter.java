package com.example.tysmaxmvvm.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.VideHolder.TopRatedTvViewHolder;
import com.example.tysmaxmvvm.Data.DataManager;
import com.example.tysmaxmvvm.Model.TvModel;
import com.example.tysmaxmvvm.R;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class TopRatedTvAdapter extends RecyclerView.Adapter<TopRatedTvViewHolder> {

    private List<TvModel> tvModelList;

    public void setTvModelList(List<TvModel> tvModelList) {
        this.tvModelList = tvModelList;
    }
    private SetOnClick SetOnClick;

    @NonNull
    @NotNull
    @Override
    public TopRatedTvViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.top_rated_tv_singlelayout, parent, false);
        return new TopRatedTvViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull TopRatedTvViewHolder holder, int position) {
        Picasso.get().load(DataManager.Imagekey+tvModelList.get(position).getPoster_path()).into(holder.imageView);
        holder.Title.setText(tvModelList.get(position).getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetOnClick.GetID(tvModelList.get(position).get_id());
            }
        });

    }

    @Override
    public int getItemCount() {
        if(tvModelList == null){
            return 0;
        }else {
            return tvModelList.size();
        }
    }

    public interface SetOnClick{
        void GetID(int id);
    }

    public void SetOnClikcLisiner(SetOnClick SetOnClick){
        this.SetOnClick = SetOnClick;
    }
}
