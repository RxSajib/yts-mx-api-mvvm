package com.example.tysmaxmvvm.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.VideHolder.TopRatingAllMovieViewHolder;
import com.example.tysmaxmvvm.Data.DataManager;
import com.example.tysmaxmvvm.Model.TmdbTopRatedMovies;
import com.example.tysmaxmvvm.Model.TmdbTopRated_Response;
import com.example.tysmaxmvvm.R;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.text.DecimalFormat;
import java.util.List;

public class TopRatingAllMovieAdapter extends RecyclerView.Adapter<TopRatingAllMovieViewHolder> {

    private List<TmdbTopRatedMovies> data;

    public void setData(List<TmdbTopRatedMovies> data) {
        this.data = data;
    }
    private SetOnClick SetOnClick;

    @NonNull
    @NotNull
    @Override
    public TopRatingAllMovieViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.toprating_allmovie_singlelayout, parent, false);
        return new TopRatingAllMovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull TopRatingAllMovieViewHolder holder, int position) {
        Picasso.get().load(DataManager.Imagekey+data.get(position).getPoster_path())
                .into(holder.imageView);

        holder.MovieTitle.setText(data.get(position).getTitle());
        holder.MoviePopularty.setText(String.valueOf(data.get(position).getVote_count()+" Vote People"));
        double val = Double.valueOf(data.get(position).getVote_average());
        holder.Rating.setText(String.valueOf(new DecimalFormat("#.#").format(val)));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetOnClick.OnClick(holder.getAdapterPosition(), data.get(position).getId(), data.get(position).getOriginal_language(),
                        data.get(position).getOverview(), data.get(position).getPopularity(), data.get(position).getPoster_path(), data.get(position).getBackdrop_path(), data.get(position).getRelease_date(),
                        data.get(position).getTitle(), data.get(position).getVote_average(), data.get(position).getVote_count());
            }
        });

    }

    @Override
    public int getItemCount() {
        if(data == null){
            return 0;
        }else {
            return data.size();
        }
    }

    public interface SetOnClick{

        void OnClick(int adapterPosition, int id, String original_language, String overview, String popularity, String poster_path, String backdrop_path, String release_date, String title, String vote_average, int vote_count);
    }

    public void SetOnclickLisiner(SetOnClick SetOnClick){
        this.SetOnClick = SetOnClick;
    }
}
