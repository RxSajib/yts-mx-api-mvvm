package com.example.tysmaxmvvm.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.VideHolder.Tmdb_Mostpopular_movieViewHolder;
import com.example.tysmaxmvvm.Data.DataManager;
import com.example.tysmaxmvvm.Model.Tmdb_Movie;
import com.example.tysmaxmvvm.R;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class TmdbMostpopular_movie_adapter extends RecyclerView.Adapter<Tmdb_Mostpopular_movieViewHolder> {

    private List<Tmdb_Movie> tmdb_mostpopularList;
    private SetOnClick SetOnClick;

    public void setTmdb_mostpopularList(List<Tmdb_Movie> tmdb_mostpopularList) {
        this.tmdb_mostpopularList = tmdb_mostpopularList;
    }

    @NonNull
    @NotNull
    @Override
    public Tmdb_Mostpopular_movieViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.popular_movie_iteams, parent, false);
        return new Tmdb_Mostpopular_movieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull Tmdb_Mostpopular_movieViewHolder holder, int position) {
        Picasso.get().load(DataManager.Imagekey+tmdb_mostpopularList.get(position).getPoster_path()).into(holder.imageView);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetOnClick.OnClick(holder.getAdapterPosition(), tmdb_mostpopularList.get(position).getId(), tmdb_mostpopularList.get(position).getOriginal_language(),
                        tmdb_mostpopularList.get(position).getOverview(), tmdb_mostpopularList.get(position).getPopularity(), tmdb_mostpopularList.get(position).getPoster_path(), tmdb_mostpopularList.get(position).getBackdrop_path(), tmdb_mostpopularList.get(position).getRelease_date(),
                        tmdb_mostpopularList.get(position).getTitle(), tmdb_mostpopularList.get(position).getVote_average(), tmdb_mostpopularList.get(position).getVote_count());
            }
        });

    }

    @Override
    public int getItemCount() {
        if(tmdb_mostpopularList == null){
            return 0;
        }else {
            return 4;
        }
    }

    public interface SetOnClick{

        void OnClick(int adapterPosition, int id, String original_language, String overview, String popularity, String poster_path, String backdrop_path, String release_date, String title, String vote_average, int vote_count);
    }

    public void SetOnclickLisiner(SetOnClick SetOnClick){
        this.SetOnClick = SetOnClick;
    }
}
