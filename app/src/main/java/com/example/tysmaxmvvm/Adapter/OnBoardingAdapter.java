package com.example.tysmaxmvvm.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.VideHolder.OnBoardingViewHolder;
import com.example.tysmaxmvvm.Model.OnBoardingModel;
import com.example.tysmaxmvvm.R;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class OnBoardingAdapter extends RecyclerView.Adapter<OnBoardingViewHolder> {

    private Context context;
    private List<OnBoardingModel> onBoardingModelList;


    public OnBoardingAdapter(Context context, List<OnBoardingModel> onBoardingModelList) {
        this.context = context;
        this.onBoardingModelList = onBoardingModelList;
    }

    @NonNull
    @NotNull
    @Override
    public OnBoardingViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.onboarding_itesm, parent, false);
        return new OnBoardingViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull OnBoardingViewHolder holder, int position) {
        holder.imageView.setImageResource(onBoardingModelList.get(position).getImage());


    }

    @Override
    public int getItemCount() {
        if(onBoardingModelList == null){
            return 0;
        }else {
            return onBoardingModelList.size();
        }
    }


}
