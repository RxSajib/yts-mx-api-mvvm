package com.example.tysmaxmvvm.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.VideHolder.Tmdb_Mostpopular_movieViewHolder;
import com.example.tysmaxmvvm.Data.DataManager;
import com.example.tysmaxmvvm.Model.TmdbTopRatedMovies;
import com.example.tysmaxmvvm.R;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class TmdbTopRatedAdapter extends RecyclerView.Adapter<Tmdb_Mostpopular_movieViewHolder> {

    private List<TmdbTopRatedMovies> data;
    private SetOnClick SetOnClick;

    public void setTopRatedMovies(List<TmdbTopRatedMovies> data) {
        this.data = data;
    }

    @NonNull
    @NotNull
    @Override
    public Tmdb_Mostpopular_movieViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.popular_movie_iteams, parent, false);
        return new Tmdb_Mostpopular_movieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull Tmdb_Mostpopular_movieViewHolder holder, int position) {
        Picasso.get().load(DataManager.Imagekey+data.get(position).getPoster_path())
                .into(holder.imageView);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetOnClick.OnClick(holder.getAdapterPosition(), data.get(position).getId(), data.get(position).getOriginal_language(),
                        data.get(position).getOverview(), data.get(position).getPopularity(), data.get(position).getPoster_path(), data.get(position).getBackdrop_path(), data.get(position).getRelease_date(),
                        data.get(position).getTitle(), data.get(position).getVote_average(), data.get(position).getVote_count());

            }
        });
    }

    @Override
    public int getItemCount() {
        if(data == null){
            return 0;
        }else {
            return 8;
        }
    }

    public interface SetOnClick{

        void OnClick(int adapterPosition, int id, String original_language, String overview, String popularity, String poster_path, String backdrop_path, String release_date, String title, String vote_average, int vote_count);
    }

    public void SetOnclickLisiner(SetOnClick SetOnClick){
        this.SetOnClick = SetOnClick;
    }
}
