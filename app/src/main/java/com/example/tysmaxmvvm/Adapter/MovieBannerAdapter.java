package com.example.tysmaxmvvm.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.VideHolder.MovieBannerViewHolder;
import com.example.tysmaxmvvm.Model.MovieBannerModel;
import com.example.tysmaxmvvm.R;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class MovieBannerAdapter extends RecyclerView.Adapter<MovieBannerViewHolder> {

    private Context context;
    private List<MovieBannerModel> movieBannerModelslist;

    public MovieBannerAdapter(Context context, List<MovieBannerModel> movieBannerModelslist) {
        this.context = context;
        this.movieBannerModelslist = movieBannerModelslist;
    }

    @NonNull
    @NotNull
    @Override
    public MovieBannerViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.movie_banner_iteam, parent, false);
        return new MovieBannerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull MovieBannerViewHolder holder, int position) {
        holder.moviebannerimage.setImageResource(movieBannerModelslist.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        if(movieBannerModelslist == null){
            return 0;
        }
       return movieBannerModelslist.size();
    }
}
