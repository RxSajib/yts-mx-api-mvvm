package com.example.tysmaxmvvm.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.VideHolder.VideoViewHolder;
import com.example.tysmaxmvvm.Model.MovieVideoModel;
import com.example.tysmaxmvvm.R;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class VideoAdapter extends RecyclerView.Adapter<VideoViewHolder> {

    private List<MovieVideoModel> videoModelList;
    private OnClick OnClick;

    public void setVideoModelList(List<MovieVideoModel> videoModelList) {
        this.videoModelList = videoModelList;
    }

    @NonNull
    @NotNull
    @Override
    public VideoViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.video_layout, parent, false);
        return new VideoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull VideoViewHolder holder, int position) {
        holder.name.setText(videoModelList.get(position).getName());
        holder.site.setText(videoModelList.get(position).getSite());
        holder.size.setText(String.valueOf(videoModelList.get(position).getSize()) +" p");

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OnClick.YoutubeKey(videoModelList.get(position).getVideokey());
            }
        });
    }

    @Override
    public int getItemCount() {
        if(videoModelList == null){
            return 0;
        }else {
            return videoModelList.size();
        }
    }

    public interface OnClick{
        void YoutubeKey(String key);
    }

    public void setOnClickLisiner(OnClick OnClick){
        this.OnClick = OnClick;
    }
}
