package com.example.tysmaxmvvm.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.VideHolder.TopRatedTvShowHolder;
import com.example.tysmaxmvvm.Data.DataManager;
import com.example.tysmaxmvvm.Model.TvModel;
import com.example.tysmaxmvvm.R;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.text.DecimalFormat;
import java.util.List;

public class TopRatedAllTvShowAdapter extends RecyclerView.Adapter<TopRatedTvShowHolder> {

    private List<TvModel> tvModelList;

    public void setTvModelList(List<TvModel> tvModelList) {
        this.tvModelList = tvModelList;
    }

    @NonNull
    @NotNull
    @Override
    public TopRatedTvShowHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.toprated_tvshow, parent, false);
        return new TopRatedTvShowHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull TopRatedTvShowHolder holder, int position) {
        Picasso.get().load(DataManager.Imagekey+tvModelList.get(position).getPoster_path()).into(holder.imageView);
        holder.Title.setText(tvModelList.get(position).getName());
        holder.Popularty.setText(tvModelList.get(position).getPopularity()+" Popularity");

        double val = Double.valueOf(tvModelList.get(position).getVote_average());
        holder.Rating.setText(String.valueOf(new DecimalFormat("#.#").format(val)));
    }

    @Override
    public int getItemCount() {
        if(tvModelList == null){
            return 0;
        }
        else {
           return tvModelList.size();
        }
    }
}
