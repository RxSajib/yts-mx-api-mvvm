package com.example.tysmaxmvvm.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.VideHolder.Tmdb_Mostpopular_movieViewHolder;
import com.example.VideHolder.TopRatingAllMovieViewHolder;
import com.example.tysmaxmvvm.Data.DataManager;
import com.example.tysmaxmvvm.Model.Tmdb_Movie;
import com.example.tysmaxmvvm.R;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.text.DecimalFormat;
import java.util.List;

public class MostPopularAllMovieAdapter extends RecyclerView.Adapter<TopRatingAllMovieViewHolder> {

    private List<Tmdb_Movie> movieList;
    private SetOnClick SetOnClick;

    public void setMovieList(List<Tmdb_Movie> movieList) {
        this.movieList = movieList;
    }

    @NonNull
    @NotNull
    @Override
    public TopRatingAllMovieViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.toprating_allmovie_singlelayout, parent, false);
        return new TopRatingAllMovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull TopRatingAllMovieViewHolder holder, int position) {
        Picasso.get().load(DataManager.Imagekey+movieList.get(position).getPoster_path())
                .into(holder.imageView);
        holder.MovieTitle.setText(movieList.get(position).getTitle());
        holder.MoviePopularty.setText(movieList.get(position).getVote_count()+" Vote People");
        double val = Double.valueOf(movieList.get(position).getVote_average());
        holder.Rating.setText(String.valueOf(new DecimalFormat("#.#").format(val)));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetOnClick.OnClick(holder.getAdapterPosition(), movieList.get(position).getId(), movieList.get(position).getOriginal_language(),
                        movieList.get(position).getOverview(), movieList.get(position).getPopularity(), movieList.get(position).getPoster_path(), movieList.get(position).getBackdrop_path(), movieList.get(position).getRelease_date(),
                        movieList.get(position).getTitle(), movieList.get(position).getVote_average(), movieList.get(position).getVote_count());

            }
        });
    }

    @Override
    public int getItemCount() {
        if(movieList == null){
            return 0;
        }else {
          return   movieList.size();
        }
    }

    public interface SetOnClick{

        void OnClick(int adapterPosition, int id, String original_language, String overview, String popularity, String poster_path, String backdrop_path, String release_date, String title, String vote_average, int vote_count);
    }

    public void SetOnclickLisiner(SetOnClick SetOnClick){
        this.SetOnClick = SetOnClick;
    }
}
