package com.example.tysmaxmvvm.Screen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.tysmaxmvvm.R;
import com.example.tysmaxmvvm.databinding.HomepageBinding;

public class HomePage extends AppCompatActivity {

    private Animation bottom_to_center;
    private HomepageBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.homepage);
        binding = DataBindingUtil.setContentView(this, R.layout.homepage);

        init_view();
        init_clickable();
        setstatusbarcolor();
        goto_moviepage(new Movie());
        page_destensction();
    }

    private void page_destensction(){
        binding.TvButtonID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goto_tvpage(new TV());
                binding.Tvicon.setImageResource(R.drawable.ic_tv_clickble);
                binding.Tvtext.setTextColor(getResources().getColor(R.color.tabclickabletext));

                binding.Movieicon.setImageResource(R.drawable.ic_movie);
                binding.MovieText.setTextColor(getResources().getColor(R.color.tab_textcolor_normal));

                binding.ToolbarTitle.startAnimation(bottom_to_center);
                bottom_to_center.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        binding.ToolbarTitle.setText("TV");
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

            }
        });

        binding.MoveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goto_moviepage(new Movie());
                binding.Tvicon.setImageResource(R.drawable.ic_tv);
                binding.Tvtext.setTextColor(getResources().getColor(R.color.tab_textcolor_normal));

                binding.Movieicon.setImageResource(R.drawable.ic_movie_clickable);
                binding.MovieText.setTextColor(getResources().getColor(R.color.tabclickabletext));
                binding.ToolbarTitle.startAnimation(bottom_to_center);
                bottom_to_center.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        binding.ToolbarTitle.setText("MOVIES");
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

            }
        });
    }

    private void init_clickable(){
        binding.Movieicon.setImageResource(R.drawable.ic_movie_clickable);
        binding.MovieText.setTextColor(getResources().getColor(R.color.tabclickabletext));
    }

    private void init_view(){
        bottom_to_center = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.bottom_to_center);
    }

    private void setstatusbarcolor(){
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M){
            getWindow().setStatusBarColor(getResources().getColor(R.color.home_background));
        } else {
            getWindow().setStatusBarColor(getResources().getColor(R.color.home_background));
        }
    }

    private void goto_moviepage(Fragment fragment){
        if(fragment != null){
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.HomeContiner, fragment);
            transaction.commit();
        }
    }

    private void goto_tvpage(Fragment fragment){
        if(fragment != null){
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.HomeContiner, fragment);
            transaction.commit();
        }
    }
}