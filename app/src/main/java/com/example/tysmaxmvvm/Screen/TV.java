package com.example.tysmaxmvvm.Screen;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ViewModel.TvShowsViewModel;
import com.example.tysmaxmvvm.Adapter.PopularTvShowsAdapter;
import com.example.tysmaxmvvm.Adapter.TmdbTopRatedAdapter;
import com.example.tysmaxmvvm.Adapter.TopRatedTvAdapter;
import com.example.tysmaxmvvm.Data.DataManager;
import com.example.tysmaxmvvm.Model.TmdbTopRatedMovies;
import com.example.tysmaxmvvm.Model.TvModel;
import com.example.tysmaxmvvm.Network.TopRateTVGET;
import com.example.tysmaxmvvm.R;
import com.example.tysmaxmvvm.Response.TopRatedTVResponse;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class TV extends Fragment {

    private RecyclerView recyclerView, popularrecylerview;
    private TvShowsViewModel tvShowsViewModel;
    private TopRatedTvAdapter adapter;
    private LinearLayoutManager linearLayoutManager;
    private List<TvModel> list = new ArrayList<>();
    private TextView TopTvshowButton, PopularAllTvShowButton;
    private PopularTvShowsAdapter popularTvShowsAdapter;
    private RelativeLayout BackButton;
    private LinearLayout PopularShimmer, TopTvShimmer;

    public TV() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_t_v, container, false);

        init_view(view);
        return view;
    }

    private void init_view(View view){
        TopTvShimmer = view.findViewById(R.id.TopTvShimmerID);
        PopularShimmer = view.findViewById(R.id.PopularShimmerID);
        tvShowsViewModel = new ViewModelProvider(this).get(TvShowsViewModel.class);
        recyclerView = view.findViewById(R.id.RecylerviewID);
        linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        adapter = new TopRatedTvAdapter();
        adapter.setTvModelList(list);
        recyclerView.setAdapter(adapter);

        TopTvshowButton = view.findViewById(R.id.TopTvShowButton);
        TopTvshowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), TopRatedTvShow_Page.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        PopularAllTvShowButton = view.findViewById(R.id.PopularTvShowButton);
        PopularAllTvShowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), PopularAllTvPage.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        getdata_server();

        popularrecylerview = view.findViewById(R.id.PopularRecylerview);
        popularrecylerview.setHasFixedSize(true);
        popularrecylerview.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        popularTvShowsAdapter = new PopularTvShowsAdapter();
        popularrecylerview.setAdapter(popularTvShowsAdapter);
    }

    private void getdata_server(){
        tvShowsViewModel.gettop_ratedTv(DataManager.TMDBAPIKEY, 2)
                .observe(getViewLifecycleOwner(), new Observer<TopRatedTVResponse>() {
                    @Override
                    public void onChanged(TopRatedTVResponse topRatedTVResponse) {

                        if(tvShowsViewModel != null){
                            list.addAll(topRatedTVResponse.getMovieList());
                              adapter.notifyDataSetChanged();
                              TopTvShimmer.setVisibility(View.GONE);

                              adapter.SetOnClikcLisiner(new TopRatedTvAdapter.SetOnClick() {
                                  @Override
                                  public void GetID(int id) {
                                      Intent intent = new Intent(getActivity(), TVShowsDetails.class);
                                      intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                      intent.putExtra(DataManager.TvID, id);
                                      startActivity(intent);
                                  }
                              });
                        }
                    }
                });

        tvShowsViewModel.tvpopular(DataManager.TMDBAPIKEY, 1).observe(getViewLifecycleOwner(), new Observer<TopRatedTVResponse>() {
            @Override
            public void onChanged(TopRatedTVResponse topRatedTVResponse) {

                if(topRatedTVResponse != null){
                    popularTvShowsAdapter.setTvModelList(topRatedTVResponse.getMovieList());
                    popularTvShowsAdapter.notifyDataSetChanged();
                    PopularShimmer.setVisibility(View.GONE);

                    popularTvShowsAdapter.SetOnClickLisiner(new PopularTvShowsAdapter.SetOnClick() {
                        @Override
                        public void OnClick(int TvID) {
                            Intent intent = new Intent(getActivity(), TVShowsDetails.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtra(DataManager.TvID, TvID);
                            startActivity(intent);
                        }
                    });
                }

            }
        });
    }
}