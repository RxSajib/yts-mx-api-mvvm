package com.example.tysmaxmvvm.Screen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ViewModel.TvShowsViewModel;
import com.example.tysmaxmvvm.Adapter.TopRatedAllTvShowAdapter;
import com.example.tysmaxmvvm.Data.DataManager;
import com.example.tysmaxmvvm.Model.TvModel;
import com.example.tysmaxmvvm.R;
import com.example.tysmaxmvvm.Response.TvDetailsResponse;
import com.example.tysmaxmvvm.Response.TvSmilerResponse;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class TVShowsDetails extends AppCompatActivity {

    private TvShowsViewModel tvShowsViewModel;
    private int TvID;
    private ImageView PosterPath, BacwordPath;
    private TextView WatchCount, RelestDate, RatingCount, Details, Title;
    private RatingBar ratingBar;
    private TopRatedAllTvShowAdapter adapter;
    private RecyclerView recyclerView;
    private NestedScrollView nestedScrollView;
    private int CurrentPage = 1, TotalPage;
    private List<TvModel> tvModelList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tvshows_details);

        statusbar_bg();
        init_view();
    }

    private void statusbar_bg(){
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M){
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }else {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    private void init_view(){
        nestedScrollView = findViewById(R.id.NsestedScrollViewID);
        adapter = new TopRatedAllTvShowAdapter();
        adapter.setTvModelList(tvModelList);
        recyclerView = findViewById(R.id.RecylerviewID);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        recyclerView.setAdapter(adapter);

        PosterPath = findViewById(R.id.PosterPathImage);
        BacwordPath = findViewById(R.id.Backdrop_pathImg);
        WatchCount = findViewById(R.id.OvteText);
        RelestDate = findViewById(R.id.RelseDateText);
        RatingCount = findViewById(R.id.RatingText);
        Details = findViewById(R.id.DetailsID);
        Title = findViewById(R.id.MovieTitle);
        ratingBar = findViewById(R.id.RatingBar);

        TvID = getIntent().getIntExtra(DataManager.TvID, 0);
        tvShowsViewModel = new ViewModelProvider(this).get(TvShowsViewModel.class);

        getdatafrom_server(TvID);
        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(scrollY == v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight()){
                    if(CurrentPage < TotalPage){
                        CurrentPage = CurrentPage+1;
                        gettv_similer(TvID, CurrentPage);
                    }
                }
            }
        });
    }

    private void getdatafrom_server(int TvID){
        tvShowsViewModel.gettv_details(TvID, DataManager.TMDBAPIKEY)
                .observe(this, new Observer<TvDetailsResponse>() {
                    @Override
                    public void onChanged(TvDetailsResponse tvDetailsResponse) {
                        if(tvDetailsResponse != null){

                            Picasso.get().load(DataManager.Imagekey+tvDetailsResponse.getPoster_path()).into(BacwordPath);
                            Picasso.get().load(DataManager.Imagekey+tvDetailsResponse.getBackdrop_path()).into(PosterPath);

                            Title.setText(tvDetailsResponse.getName());
                            Details.setText(tvDetailsResponse.getOverview());
                            RelestDate.setText(tvDetailsResponse.getFirst_air_date());
                            WatchCount.setText(String.valueOf(tvDetailsResponse.getNumber_of_episodes())+" Episodes");
                            double val = Double.valueOf(tvDetailsResponse.getVote_average());
                            RatingCount.setText(String.valueOf(new DecimalFormat("#.#").format(val)));

                            float count = Float.valueOf(tvDetailsResponse.getVote_average()) /2;
                            ratingBar.setRating(count);

                            gettv_similer(tvDetailsResponse.getId(), CurrentPage);

                        }
                    }
                });
    }

    private void gettv_similer(int TvID, int CurrentPage){
        tvShowsViewModel.get_tvsimiler(TvID, DataManager.TMDBAPIKEY, CurrentPage)
                .observe(this, new Observer<TvSmilerResponse>() {
                    @Override
                    public void onChanged(TvSmilerResponse tvSmilerResponse) {
                        if(tvSmilerResponse != null){
                            tvModelList.addAll(tvSmilerResponse.getTvModelList());
                            TotalPage = tvSmilerResponse.getTotal_pages();
                            adapter.notifyDataSetChanged();
                        }
                    }
                });
    }
}