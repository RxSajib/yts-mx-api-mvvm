package com.example.tysmaxmvvm.Screen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.ViewModel.MovieViewModel;
import com.example.tysmaxmvvm.Adapter.RecommendationAdapter;
import com.example.tysmaxmvvm.Adapter.VideoAdapter;
import com.example.tysmaxmvvm.Data.DataManager;
import com.example.tysmaxmvvm.Model.Recommendation_Response;
import com.example.tysmaxmvvm.Model.Tmdb_Movie;
import com.example.tysmaxmvvm.R;
import com.example.tysmaxmvvm.Response.MovieVideoResponse;
import com.example.tysmaxmvvm.databinding.ActivitymoviedetailsBinding;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MovieDetails extends AppCompatActivity {

    private ImageView posterpath, backdrop_path_img;
    private int position, page, MovieID, VoteCount;
    private MovieViewModel viewModel;
    private String Poster_path, Backdrop_path, Overview, Title, Date, Rating;
    private TextView MovieDetils, MovieTitle, VoteCountText, RatingText, RelseDateText;
    private RecyclerView recyclerView;
    private RecommendationAdapter adapter;
    private MaterialToolbar Mtoolbar;
    private StaggeredGridLayoutManager staggeredGridLayoutManager;
    private RatingBar ratingBar;
    private int CurrentPage = 1;
    private int TotalPage;
    private NestedScrollView nestedScrollView;
    private List<Tmdb_Movie> movieList = new ArrayList<>();
    private ImageView PlayButton;
    private VideoAdapter videoAdapter;
    private ProgressDialog Mprogressdialoag;
    private ActivitymoviedetailsBinding binding;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activitymoviedetails);

        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M){
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }else {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        init_view();
        getdata_server();

    }

    private void getvideo(){
        Mprogressdialoag.show();
        viewModel.getmovie_video(MovieID, DataManager.TMDBAPIKEY)
                .observe(this, new Observer<MovieVideoResponse>() {
                    @Override
                    public void onChanged(MovieVideoResponse movieVideoResponse) {
                        if(movieVideoResponse != null){
                            BottomSheetDialog Mdialoag = new BottomSheetDialog(MovieDetails.this);
                            View view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.video_list, null, false);
                            Mdialoag.setContentView(view);

                            Mdialoag.show();

                            RecyclerView recyclerView = view.findViewById(R.id.RecylerviewID);
                            recyclerView.setHasFixedSize(true);
                            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                            videoAdapter = new VideoAdapter();
                            recyclerView.setAdapter(videoAdapter);

                            videoAdapter.setVideoModelList(movieVideoResponse.getMovieVideoModelList());
                            videoAdapter.notifyDataSetChanged();
                            Mprogressdialoag.dismiss();
                            videoAdapter.setOnClickLisiner(new VideoAdapter.OnClick() {
                                @Override
                                public void YoutubeKey(String key) {
                                    Intent intent = new Intent(getApplicationContext(), VideoView.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    intent.putExtra(DataManager.VideoKey, key);
                                    startActivity(intent);
                                }
                            });
                        }
                    }
                });
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    private void init_view(){

        Mprogressdialoag = new ProgressDialog(MovieDetails.this);
        Mprogressdialoag.setMessage("Please wait ...");

        PlayButton = findViewById(R.id.PlayButtonID);
        PlayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getvideo();
            }
        });

        nestedScrollView = findViewById(R.id.NsestedScrollViewID);
        ratingBar = findViewById(R.id.RatingBar);
        recyclerView = findViewById(R.id.RecylerviewID);

        recyclerView.setHasFixedSize(true);
        staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        adapter = new RecommendationAdapter();
        recyclerView.setAdapter(adapter);
        adapter.setMovieList(movieList);

        MovieDetils = findViewById(R.id.DetailsID);
        backdrop_path_img = findViewById(R.id.Backdrop_pathImg);
        viewModel = new ViewModelProvider(this).get(MovieViewModel.class);
        posterpath = findViewById(R.id.PosterPathImage);
    }

    private void getdata_server(){
        RelseDateText = findViewById(R.id.RelseDateText);
        MovieTitle = findViewById(R.id.MovieTitle);
        VoteCountText = findViewById(R.id.OvteText);
        RatingText = findViewById(R.id.RatingText);
        position = getIntent().getIntExtra(DataManager.Position, 0);
        page = getIntent().getIntExtra(DataManager.Page, 0);
        MovieID = getIntent().getIntExtra(DataManager.MovieID, 0);
        Poster_path = getIntent().getStringExtra(DataManager.PosterPath);
        Backdrop_path = getIntent().getStringExtra(DataManager.Backdrop_path);
        Overview = getIntent().getStringExtra(DataManager.Overview);
        Title = getIntent().getStringExtra(DataManager.MovieTitle);
        VoteCount = getIntent().getIntExtra(DataManager.Vote_count, 0);
        Date = getIntent().getStringExtra(DataManager.Release_date);
        Rating = getIntent().getStringExtra(DataManager.Vote_average);

        Picasso.get().load(DataManager.Imagekey+Poster_path).into(posterpath);
        Picasso.get().load(DataManager.Imagekey+Backdrop_path).into(backdrop_path_img);
        MovieDetils.setText(Overview);
        MovieTitle.setText(Title);
        VoteCountText.setText(String.valueOf(VoteCount)+" People Vote");
        RelseDateText.setText(Date);
        RatingText.setText(Rating);
        float v = Float.valueOf(Rating)/2;
        ratingBar.setRating(v);

        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(scrollY == v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight()){
                    if(CurrentPage <= TotalPage){

                        CurrentPage = CurrentPage+1;
                        getrecomdation_movie(CurrentPage);
                    }
                }
            }
        });

        getrecomdation_movie(CurrentPage);
    }

    private void getrecomdation_movie(int page){
        viewModel.getrecomandation_movie(MovieID, DataManager.TMDBAPIKEY, page).observe(this, new Observer<Recommendation_Response>() {
            @Override
            public void onChanged(Recommendation_Response recommendation_response) {

                if(recommendation_response != null){
                    TotalPage = recommendation_response.getTotal_pages();
                    movieList.addAll(recommendation_response.getMovieList());
                    adapter.notifyDataSetChanged();

                    adapter.SetOnclickLisiner(new RecommendationAdapter.SetOnClick() {
                        @Override
                        public void OnClick(int adapterPosition, int id, String original_language, String overview, String popularity, String poster_path, String backdrop_path, String release_date, String title, String vote_average, int vote_count) {
                            Intent intent = new Intent(getApplicationContext(), MovieDetails.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtra(DataManager.Position, adapterPosition);
                            intent.putExtra(DataManager.MovieID, id);
                            intent.putExtra(DataManager.PosterPath, poster_path);
                            intent.putExtra(DataManager.Backdrop_path, backdrop_path);
                            intent.putExtra(DataManager.Overview, overview);
                            intent.putExtra(DataManager.MovieTitle, title);
                            intent.putExtra(DataManager.Vote_count, vote_count);
                            intent.putExtra(DataManager.Vote_average, vote_average);
                            intent.putExtra(DataManager.Release_date, release_date);
                            startActivity(intent);
                        }
                    });
                }

            }
        });
    }
}