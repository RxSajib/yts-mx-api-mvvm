package com.example.tysmaxmvvm.Screen;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.example.ViewModel.TvShowsViewModel;
import com.example.tysmaxmvvm.Adapter.PopularAllTVAdapter;
import com.example.tysmaxmvvm.Data.DataManager;
import com.example.tysmaxmvvm.Model.TvModel;
import com.example.tysmaxmvvm.R;
import com.example.tysmaxmvvm.Response.TopRatedTVResponse;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class PopularAllTvPage extends AppCompatActivity {

    private RecyclerView recyclerView;
    private PopularAllTVAdapter adapter;
    private List<TvModel> modelList = new ArrayList<>();
    private TvShowsViewModel tvShowsViewModel;
    private int CurrentPage = 1;
    private int TotalPage;
    private RelativeLayout BackButton;
    private LinearLayout ShimmerAni;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popular_all_tv_page);

        set_starusbar_bg();
        init_view();
    }

    private void set_starusbar_bg(){
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M){
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_white));
        }else {
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_white));
        }
    }

    private void init_view(){
        ShimmerAni = findViewById(R.id.ShimmerAnimactionID);
        BackButton = findViewById(R.id.backButtonID);
        BackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        tvShowsViewModel = new ViewModelProvider(this).get(TvShowsViewModel.class);
        recyclerView = findViewById(R.id.RecylerviewID);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        adapter = new PopularAllTVAdapter();
        adapter.setTvModelList(modelList);
        recyclerView.setAdapter(adapter);

        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull @NotNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(!recyclerView.canScrollVertically(1)){
                    CurrentPage = CurrentPage+1;
                    getdata_server(CurrentPage);
                }
            }
        });
        getdata_server(CurrentPage);
    }

    private void getdata_server(int Page){
        tvShowsViewModel.tvpopular(DataManager.TMDBAPIKEY, Page)
                .observe(this, new Observer<TopRatedTVResponse>() {
                    @Override
                    public void onChanged(TopRatedTVResponse topRatedTVResponse) {
                        if(topRatedTVResponse != null){
                            TotalPage = topRatedTVResponse.getTotal_pages();
                            modelList.addAll(topRatedTVResponse.getMovieList());
                            adapter.notifyDataSetChanged();
                            ShimmerAni.setVisibility(View.GONE);

                            adapter.SetOnClickLisiner(new PopularAllTVAdapter.OnClick() {
                                @Override
                                public void Click(int TvID) {
                                    Intent intent = new Intent(getApplicationContext(), TVShowsDetails.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    intent.putExtra(DataManager.TvID, TvID);
                                    startActivity(intent);
                                }
                            });
                        }
                    }
                });
    }
}