package com.example.tysmaxmvvm.Screen;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.example.ViewModel.TvShowsViewModel;
import com.example.tysmaxmvvm.Adapter.TvAdapter;
import com.example.tysmaxmvvm.Data.DataManager;
import com.example.tysmaxmvvm.Model.TvModel;
import com.example.tysmaxmvvm.Network.TopRateTVGET;
import com.example.tysmaxmvvm.R;
import com.example.tysmaxmvvm.Response.TopRatedTVResponse;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class TopRatedTvShow_Page extends AppCompatActivity {

    private RelativeLayout BackButton;
    private RecyclerView recyclerView;
    private TvAdapter tvAdapter;
    private List<TvModel> tvModelList = new ArrayList<>();
    private TvShowsViewModel tvShowsViewModel;
    private int CurrentPage = 1;
    private int TotalPage;
    private LinearLayout ShimmerAni;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.top_rated_tv_show_page);

        set_statusbarcolor();
        init_view();
    }

    private void set_statusbarcolor(){
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M){
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_white));
        }else {
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_white));
        }
    }

    private void init_view(){
        ShimmerAni = findViewById(R.id.ShimmerAnimactionID);
        tvShowsViewModel = new ViewModelProvider(this).get(TvShowsViewModel.class);
        BackButton = findViewById(R.id.backButtonID);
        BackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        recyclerView = findViewById(R.id.RecylerviewID);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        tvAdapter = new TvAdapter();
        tvAdapter.setTvModelList(tvModelList);
        recyclerView.setAdapter(tvAdapter);

        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull @NotNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if(!recyclerView.canScrollVertically(1)){
                    if(CurrentPage < TotalPage){
                        CurrentPage = CurrentPage+1;
                        gettvhow(CurrentPage);
                    }
                }

            }
        });

        gettvhow(CurrentPage);
    }

    private void gettvhow(int Page){
        tvShowsViewModel.gettop_ratedTv(DataManager.TMDBAPIKEY, Page)
                .observe(this, new Observer<TopRatedTVResponse>() {
                    @Override
                    public void onChanged(TopRatedTVResponse topRatedTVResponse) {
                        if(topRatedTVResponse != null){
                            TotalPage= topRatedTVResponse.getTotal_pages();
                            tvModelList.addAll(topRatedTVResponse.getMovieList());
                            tvAdapter.notifyDataSetChanged();
                            ShimmerAni.setVisibility(View.GONE);

                            tvAdapter.SetOnclicLisiner(new TvAdapter.OnClick() {
                                @Override
                                public void Click(int TvID) {
                                    Intent intent = new Intent(getApplicationContext(), TVShowsDetails.class);
                                    intent.putExtra(DataManager.TvID, TvID);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                }
                            });
                        }

                    }
                });
    }
}