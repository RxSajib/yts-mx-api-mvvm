package com.example.tysmaxmvvm.Screen;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.ViewModel.MovieViewModel;
import com.example.tysmaxmvvm.Adapter.TopRatingAllMovieAdapter;
import com.example.tysmaxmvvm.Data.DataManager;
import com.example.tysmaxmvvm.Model.TmdbTopRatedMovies;
import com.example.tysmaxmvvm.Model.TmdbTopRated_Response;
import com.example.tysmaxmvvm.R;
import com.example.tysmaxmvvm.Room.TopRatedMovieDio;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class TopRatedPage extends AppCompatActivity {

    private RelativeLayout backbutton;
    private MovieViewModel viewModel;
    private RecyclerView recyclerView;
    private TopRatingAllMovieAdapter adapter;
    private LinearLayout ShimmerAni;
    private SwipeRefreshLayout swipeRefreshLayout;
    private int currentpage = 1;
    private int totalpage;
    private List<TmdbTopRatedMovies> mymovielist = new ArrayList<>();
    private StaggeredGridLayoutManager staggeredGridLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_rated_page);

        init_view();
        setstatusbarcolor();
    }

    private void setstatusbarcolor(){
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M){
            getWindow().setStatusBarColor(getResources().getColor(R.color.white));
        }else {
            getWindow().setStatusBarColor(getResources().getColor(R.color.white));
        }
    }

    private void init_view(){
        viewModel = new ViewModelProvider(this).get(MovieViewModel.class);

        swipeRefreshLayout = findViewById(R.id.SwipeRefreshlayoutID);
        ShimmerAni = findViewById(R.id.ShimmerAnimactionID);
        backbutton = findViewById(R.id.backButtonID);
        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        recyclerView = findViewById(R.id.RecylerviewID);
        recyclerView.setHasFixedSize(true);
        staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(staggeredGridLayoutManager);
        adapter = new TopRatingAllMovieAdapter();
        adapter.setData(mymovielist);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull @NotNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if(!recyclerView.canScrollVertically(1)){
                    if(currentpage <= totalpage){
                        currentpage +=1;
                        geall_topratedmovies(currentpage);
                    }
                }
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                geall_topratedmovies(currentpage);
            }
        });
        geall_topratedmovies(currentpage);
    }

    private void geall_topratedmovies(int Page){
        swipeRefreshLayout.setRefreshing(true);
        viewModel.getalltopratedmovies(DataManager.TMDBAPIKEY, Page)
                .observe(this, new Observer<TmdbTopRated_Response>() {
                    @Override
                    public void onChanged(TmdbTopRated_Response tmdbTopRated_response) {

                        if(tmdbTopRated_response != null){
                            totalpage = tmdbTopRated_response.getTotal_pages();

                           viewModel.insert_topratedmovie(tmdbTopRated_response.getTopratedlist());

                          /*  mymovielist.addAll(tmdbTopRated_response.getTopratedlist());
                            adapter.notifyDataSetChanged();
                            ShimmerAni.setVisibility(View.GONE);
                            swipeRefreshLayout.setRefreshing(false);
*/

                        }
                        else {
                            swipeRefreshLayout.setRefreshing(false);
                        }

                    }
                });

        get_topratedmovie();
    }

    private void get_topratedmovie(){

        viewModel.get_topratedmovielist().observe(this, new Observer<List<TmdbTopRatedMovies>>() {
            @Override
            public void onChanged(List<TmdbTopRatedMovies> list) {
                if(list != null){

                    mymovielist.addAll(list);
                    adapter.notifyDataSetChanged();
                    ShimmerAni.setVisibility(View.GONE);
                    swipeRefreshLayout.setRefreshing(false);
                    Log.d("Size", String.valueOf(list.size()));


                    adapter.SetOnclickLisiner(new TopRatingAllMovieAdapter.SetOnClick() {
                        @Override
                        public void OnClick(int adapterPosition, int id, String original_language, String overview, String popularity, String poster_path,String backdrop_path, String release_date, String title, String vote_average, int vote_count) {
                            Intent intent = new Intent(getApplicationContext(), MovieDetails.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtra(DataManager.Position, adapterPosition);
                            intent.putExtra(DataManager.MovieID, id);
                            intent.putExtra(DataManager.PosterPath, poster_path);
                            intent.putExtra(DataManager.Backdrop_path, backdrop_path);
                            intent.putExtra(DataManager.Overview, overview);
                            intent.putExtra(DataManager.MovieTitle, title);
                            intent.putExtra(DataManager.Vote_count, vote_count);
                            intent.putExtra(DataManager.Vote_average, vote_average);
                            intent.putExtra(DataManager.Release_date, release_date);
                            startActivity(intent);
                        }
                    });

                }
            }
        });
    }
}