package com.example.tysmaxmvvm.Screen;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.ViewModel.MovieViewModel;
import com.example.tysmaxmvvm.Adapter.MostPopularAllMovieAdapter;
import com.example.tysmaxmvvm.Data.DataManager;
import com.example.tysmaxmvvm.Model.TmdbMostpopularResponse;
import com.example.tysmaxmvvm.Model.Tmdb_Movie;
import com.example.tysmaxmvvm.R;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class PopularPage extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RelativeLayout Backbutton;
    private MovieViewModel viewModel;
    private MostPopularAllMovieAdapter adapter;
    private List<Tmdb_Movie> movieList = new ArrayList<>();
    private LinearLayout ShimmerAnim;
    private ProgressBar progressBar;
    private int PageNo = 1;
    private int TotalPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.popular_page);

        statusbar_bg();
        init_view();

    }

    private void init_view(){
        progressBar = findViewById(R.id.Progressbar);
        ShimmerAnim =  findViewById(R.id.ShimmerAnimactionID);
        viewModel = new ViewModelProvider(this).get(MovieViewModel.class);
        Backbutton = findViewById(R.id.backButtonID);
        Backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        recyclerView = findViewById(R.id.RecylerviewID);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        adapter = new MostPopularAllMovieAdapter();
        adapter.setMovieList(movieList);
        recyclerView.setAdapter(adapter);

        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull @NotNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(!recyclerView.canScrollVertically(1)){
                    progressBar.setVisibility(View.VISIBLE);
                    PageNo = PageNo+1;
                    getdata_sserver(PageNo);

                }
            }
        });

        getdata_sserver(PageNo);
    }

    private void statusbar_bg(){
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.M){
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_white));
        }else {
            getWindow().setStatusBarColor(getResources().getColor(R.color.carbon_white));
        }
    }

    private void getdata_sserver(int pageno){
        viewModel.gettmbd_latestmovies(DataManager.TMDBAPIKEY, pageno)
                .observe(this, new Observer<TmdbMostpopularResponse>() {
                    @Override
                    public void onChanged(TmdbMostpopularResponse tmdbMostpopularResponse) {
                        if(tmdbMostpopularResponse != null){
                            TotalPage = tmdbMostpopularResponse.getTotal_pages();
                            movieList.addAll(tmdbMostpopularResponse.getTmdb_mostpopulars());
                            adapter.notifyDataSetChanged();
                            ShimmerAnim.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);

                            adapter.SetOnclickLisiner(new MostPopularAllMovieAdapter.SetOnClick() {
                                @Override
                                public void OnClick(int adapterPosition, int id, String original_language, String overview, String popularity, String poster_path, String backdrop_path, String release_date, String title, String vote_average, int vote_count) {
                                    Intent intent = new Intent(getApplicationContext(), MovieDetails.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    intent.putExtra(DataManager.Position, adapterPosition);
                                    intent.putExtra(DataManager.MovieID, id);
                                    intent.putExtra(DataManager.PosterPath, poster_path);
                                    intent.putExtra(DataManager.Backdrop_path, backdrop_path);
                                    intent.putExtra(DataManager.Overview, overview);
                                    intent.putExtra(DataManager.MovieTitle, title);
                                    intent.putExtra(DataManager.Vote_count, vote_count);
                                    intent.putExtra(DataManager.Vote_average, vote_average);
                                    intent.putExtra(DataManager.Release_date, release_date);
                                    startActivity(intent);
                                }
                            });
                        }
                    }
                });
    }
}