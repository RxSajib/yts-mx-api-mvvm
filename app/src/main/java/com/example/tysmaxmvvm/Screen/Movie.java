package com.example.tysmaxmvvm.Screen;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.ViewModel.MovieViewModel;
import com.example.tysmaxmvvm.Adapter.MovieBannerAdapter;
import com.example.tysmaxmvvm.Adapter.TmdbMostpopular_movie_adapter;
import com.example.tysmaxmvvm.Adapter.TmdbTopRatedAdapter;
import com.example.tysmaxmvvm.Data.DataManager;
import com.example.tysmaxmvvm.Model.MovieBannerModel;
import com.example.tysmaxmvvm.Model.TmdbMostpopularResponse;
import com.example.tysmaxmvvm.Model.TmdbTopRatedMovies;
import com.example.tysmaxmvvm.Model.TmdbTopRated_Response;
import com.example.tysmaxmvvm.Model.Tmdb_Movie;
import com.example.tysmaxmvvm.R;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;


public class Movie extends Fragment {

    private MovieViewModel allMovieViewHolder;
    private ViewPager2 viewPager2;
    private List<MovieBannerModel> movieBannerModelList = new ArrayList<>();
    private MovieBannerAdapter movieBannerAdapter;
    private LinearLayout Onboardingdot;
    private MovieViewModel allMovieViewModel;
    private RecyclerView recyclerView;
    private TmdbMostpopular_movie_adapter adapter;
    private List<Tmdb_Movie> tmdb_mostpopularList = new ArrayList<>();

    private RecyclerView TopratedRecylerview;
    private List<TmdbTopRatedMovies> topRatedMovieslist = new ArrayList<>();
    private TmdbTopRatedAdapter tmdbTopRatedAdapter;
    private LinearLayoutManager linearLayoutManager;
    private LinearLayout TopRatedShimmer, PopularShimmerID;

    private TextView TopRatedButton, PopularBtn;

    public Movie() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_movie, container, false);

        init_view(view);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TopRatedButton = view.findViewById(R.id.TopratedButtonID);
        TopRatedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goto_topratedpage();
            }
        });

        PopularShimmerID = view.findViewById(R.id.PopularShimmerID);
        TopRatedShimmer = view.findViewById(R.id.TopRatedShimmerID);
        recyclerView = view.findViewById(R.id.MostPopularMovieRecylerviewID);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        adapter = new TmdbMostpopular_movie_adapter();
        recyclerView.setAdapter(adapter);


        allMovieViewModel = new ViewModelProvider(this).get(MovieViewModel.class);
        allMovieViewModel.gettmbd_latestmovies(DataManager.TMDBAPIKEY, 1).observe(getViewLifecycleOwner(), new Observer<TmdbMostpopularResponse>() {
            @Override
            public void onChanged(TmdbMostpopularResponse tmdbMostpopularResponse) {
                tmdb_mostpopularList.addAll(tmdbMostpopularResponse.getTmdb_mostpopulars());
                adapter.setTmdb_mostpopularList(tmdb_mostpopularList);
                adapter.notifyDataSetChanged();
                PopularShimmerID.setVisibility(View.GONE);

                adapter.SetOnclickLisiner(new TmdbMostpopular_movie_adapter.SetOnClick() {
                    @Override
                    public void OnClick(int adapterPosition, int id, String original_language, String overview, String popularity, String poster_path, String backdrop_path, String release_date, String title, String vote_average, int vote_count) {
                        Intent intent = new Intent(getActivity(), MovieDetails.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra(DataManager.Position, adapterPosition);
                        intent.putExtra(DataManager.MovieID, id);
                        intent.putExtra(DataManager.PosterPath, poster_path);
                        intent.putExtra(DataManager.Backdrop_path, backdrop_path);
                        intent.putExtra(DataManager.Overview, overview);
                        intent.putExtra(DataManager.MovieTitle, title);
                        intent.putExtra(DataManager.Vote_count, vote_count);
                        intent.putExtra(DataManager.Vote_average, vote_average);
                        intent.putExtra(DataManager.Release_date, release_date);
                        startActivity(intent);
                    }
                });
            }
        });


        TopratedRecylerview = view.findViewById(R.id.TopratedRecylerviewID);
        TopratedRecylerview.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        TopratedRecylerview.setLayoutManager(linearLayoutManager);
        tmdbTopRatedAdapter = new TmdbTopRatedAdapter();
        TopratedRecylerview.setAdapter(tmdbTopRatedAdapter);
        tmdbTopRatedAdapter.SetOnclickLisiner(new TmdbTopRatedAdapter.SetOnClick() {
            @Override
            public void OnClick(int adapterPosition, int id, String original_language, String overview, String popularity, String poster_path, String backdrop_path, String release_date, String title, String vote_average, int vote_count) {
                Intent intent = new Intent(getActivity(), MovieDetails.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(DataManager.Position, adapterPosition);
                intent.putExtra(DataManager.MovieID, id);
                intent.putExtra(DataManager.PosterPath, poster_path);
                intent.putExtra(DataManager.Backdrop_path, backdrop_path);
                intent.putExtra(DataManager.Overview, overview);
                intent.putExtra(DataManager.MovieTitle, title);
                intent.putExtra(DataManager.Vote_count, vote_count);
                intent.putExtra(DataManager.Vote_average, vote_average);
                intent.putExtra(DataManager.Release_date, release_date);
                startActivity(intent);
            }
        });

        allMovieViewModel.gettoprated_movies(DataManager.TMDBAPIKEY).observe(getViewLifecycleOwner(), new Observer<TmdbTopRated_Response>() {
            @Override
            public void onChanged(TmdbTopRated_Response tmdbTopRated_response) {
                topRatedMovieslist.addAll(tmdbTopRated_response.getTopratedlist());
                tmdbTopRatedAdapter.setTopRatedMovies(topRatedMovieslist);
                tmdbTopRatedAdapter.notifyDataSetChanged();
                TopRatedShimmer.setVisibility(View.GONE);
            }
        });
    }

    private void init_view(View view){
        PopularBtn = view.findViewById(R.id.PopularAllButton);
        PopularBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), PopularPage.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        });

        viewPager2 = view.findViewById(R.id.ViewPagerID);
        Onboardingdot = view.findViewById(R.id.OnBoardingDots);
        MovieBannerModel one = new MovieBannerModel();
        one.setImage(R.drawable.movie_banner_one);

        MovieBannerModel two = new MovieBannerModel();
        two.setImage(R.drawable.movie_banner_two);

        MovieBannerModel three = new MovieBannerModel();
        three.setImage(R.drawable.movie_banner_three);

        movieBannerModelList.add(one);
        movieBannerModelList.add(two);
        movieBannerModelList.add(three);

        movieBannerAdapter = new MovieBannerAdapter(getActivity(), movieBannerModelList);
        viewPager2.setAdapter(movieBannerAdapter);


        setuponboardingIncodors();
        setcurrentOnboadingIndicators(0);
        viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                setcurrentOnboadingIndicators(position);
            }
        });
    }


    private void setcurrentOnboadingIndicators(int index) {
        int childcount = Onboardingdot.getChildCount();

        for (int i = 0; i < childcount; i++) {
            ImageView imageView = (ImageView) Onboardingdot.getChildAt(i);
            if (i == index) {
                imageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.tab_selected));





            } else {
                imageView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.tab_unselected));

            }
        }

        if (index == movieBannerAdapter.getItemCount() - 1) {
          /*  nextbutton.setImageResource(R.drawable.get_started);
            nextbutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goto_homepage();
                }
            });*/

        } else {
         //   nextbutton.setImageResource(R.drawable.next_icon);
        }
    }

    private void setuponboardingIncodors() {
        ImageView[] indicatore = new ImageView[movieBannerAdapter.getItemCount()];
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT
        );

        layoutParams.setMargins(8, 0, 8, 0);
        for (int i = 0; i < indicatore.length; i++) {
            indicatore[i] = new ImageView(getActivity());
            indicatore[i].setImageDrawable(ContextCompat.getDrawable(getActivity(),
                    R.drawable.inactive_dot));

            indicatore[i].setLayoutParams(layoutParams);
            Onboardingdot.addView(indicatore[i]);
        }

    }

    private void goto_topratedpage(){
        Intent intent = new Intent(getActivity(), TopRatedPage.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
       // Animatoo.animateSlideLeft(getActivity());
    }
}