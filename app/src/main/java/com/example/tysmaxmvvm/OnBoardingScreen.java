package com.example.tysmaxmvvm;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager2.widget.ViewPager2;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.tysmaxmvvm.Adapter.OnBoardingAdapter;
import com.example.tysmaxmvvm.Model.OnBoardingModel;
import com.example.tysmaxmvvm.Screen.HomePage;

import java.util.ArrayList;
import java.util.List;

public class OnBoardingScreen extends AppCompatActivity {

    private OnBoardingAdapter onBoardingAdapter;
    private ViewPager2 viewPager2;
    private List<OnBoardingModel> onBoardingModelList = new ArrayList<>();
    private ImageView nextbutton;
    private LinearLayout Onboardingdot;
    private TextView textView;
    private Animation fadinanimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.on_boarding_screen);

        statusbar();
        init_view();
    }

    private void init_view() {
        fadinanimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadin_animaction);
        textView = findViewById(R.id.Title);
        Onboardingdot = findViewById(R.id.OnBoardingDots);

        nextbutton = findViewById(R.id.NextButtonID);
        viewPager2 = findViewById(R.id.ViewPager);
        OnBoardingModel one = new OnBoardingModel();
        one.setImage(R.drawable.onboarding_screen_one);

        OnBoardingModel two = new OnBoardingModel();
        two.setImage(R.drawable.onboarding_scren_two);

        OnBoardingModel three = new OnBoardingModel();
        three.setImage(R.drawable.onboarding_screen_three);

        onBoardingModelList.add(one);
        onBoardingModelList.add(two);
        onBoardingModelList.add(three);


        onBoardingAdapter = new OnBoardingAdapter(getApplicationContext(), onBoardingModelList);
        viewPager2.setAdapter(onBoardingAdapter);


        setuponboardingIncodors();
        setcurrentOnboadingIndicators(0);
        viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                setcurrentOnboadingIndicators(position);
            }
        });

        nextbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewPager2.getCurrentItem() + 1 < onBoardingAdapter.getItemCount()) {
                    viewPager2.setCurrentItem(viewPager2.getCurrentItem() + 1);
                } else {
                    // goto home page
                    //    goto_loginpage(new LoginPage());
                }
            }
        });
    }

    private void statusbar() {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        } else {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }


    private void setuponboardingIncodors() {
        ImageView[] indicatore = new ImageView[onBoardingAdapter.getItemCount()];
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT
        );

        layoutParams.setMargins(8, 0, 8, 0);
        for (int i = 0; i < indicatore.length; i++) {
            indicatore[i] = new ImageView(getApplicationContext());
            indicatore[i].setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),
                    R.drawable.inactive_dot));

            indicatore[i].setLayoutParams(layoutParams);
            Onboardingdot.addView(indicatore[i]);
        }

    }


    private void setcurrentOnboadingIndicators(int index) {
        int childcount = Onboardingdot.getChildCount();

        for (int i = 0; i < childcount; i++) {
            ImageView imageView = (ImageView) Onboardingdot.getChildAt(i);
            if (i == index) {
                imageView.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.active_dot));

                if (index == 0) {
                    textView.startAnimation(fadinanimation);
                    fadinanimation.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                            textView.setText(R.string.onboarding_first_text);
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });

                } else if (index == 1) {
                    textView.startAnimation(fadinanimation);
                    fadinanimation.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                            textView.setText(R.string.onboarding_second_text);
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });

                } else if (index == 2) {
                    textView.startAnimation(fadinanimation);
                    fadinanimation.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                            textView.setText(R.string.onboarding_third_text);
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {

                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });


                }
            } else {
                imageView.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.inactive_dot));

            }
        }

        if (index == onBoardingAdapter.getItemCount() - 1) {
            nextbutton.setImageResource(R.drawable.get_started);
            nextbutton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goto_homepage();
                }
            });

        } else {
            nextbutton.setImageResource(R.drawable.next_icon);
        }
    }

    private void goto_homepage(){
        Intent intent = new Intent(getApplicationContext(), HomePage.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}