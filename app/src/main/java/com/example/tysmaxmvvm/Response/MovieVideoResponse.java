package com.example.tysmaxmvvm.Response;

import com.example.tysmaxmvvm.Model.MovieVideoModel;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MovieVideoResponse {

    @SerializedName("id")
    private int MovieID;

    @SerializedName("results")
    private List<MovieVideoModel> movieVideoModelList;

    public int getMovieID() {
        return MovieID;
    }

    public void setMovieID(int movieID) {
        MovieID = movieID;
    }

    public List<MovieVideoModel> getMovieVideoModelList() {
        return movieVideoModelList;
    }

    public void setMovieVideoModelList(List<MovieVideoModel> movieVideoModelList) {
        this.movieVideoModelList = movieVideoModelList;
    }
}
