package com.example.tysmaxmvvm.Response;

import com.example.tysmaxmvvm.Model.Movie;
import com.example.tysmaxmvvm.Model.TmdbTopRatedMovies;
import com.example.tysmaxmvvm.Model.TvModel;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TopRatedTVResponse {

    @SerializedName("page")
    private int page;

    @SerializedName("total_pages")
    private int total_pages;

    @SerializedName("total_results")
    private int total_results;

    @SerializedName("results")
    private List<TvModel> movieList;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotal_pages() {
        return total_pages;
    }

    public void setTotal_pages(int total_pages) {
        this.total_pages = total_pages;
    }

    public int getTotal_results() {
        return total_results;
    }

    public void setTotal_results(int total_results) {
        this.total_results = total_results;
    }

    public List<TvModel> getMovieList() {
        return movieList;
    }

    public void setMovieList(List<TvModel> movieList) {
        this.movieList = movieList;
    }
}
