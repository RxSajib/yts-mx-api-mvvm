package com.example.tysmaxmvvm.Response;

import com.example.tysmaxmvvm.Model.MovieData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MovieResponse {

    @SerializedName("data")
    @Expose
    private MovieData movieData;


    public MovieData getMovieData() {
        return movieData;
    }

    public void setMovieData(MovieData movieData) {
        this.movieData = movieData;
    }
}
