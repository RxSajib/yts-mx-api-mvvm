package com.example.tysmaxmvvm.Data;

public class DataManager {

    public static final String BASEURI = "https://yts.mx/api/v2/";

    public static final String TMDBBASEURL = "https://api.themoviedb.org/3/";
    public static final String TMDBAPIKEY = "f8536c3eafdfbbb3bce381683a2f905f";
    public static final String Imagekey = "https://image.tmdb.org/t/p/w500";

    public static final String Position = "Position";
    public static final String Page = "Page";
    public static final String MovieID = "MovieID";
    public static final String PosterPath = "PosterPath";
    public static final String Backdrop_path  = "Backdrop_path";
    public static final String Overview  = "Overview";
    public static final String MovieTitle  = "MovieTitle";
    public static final String Vote_count  = "Vote_count";
    public static final String Vote_average  = "Vote_average";
    public static final String Release_date  = "Release_date";
    public static final String TvID = "TVID";
    public static final String VideoKey = "VideoKey";
}
