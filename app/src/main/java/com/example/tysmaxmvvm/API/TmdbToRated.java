package com.example.tysmaxmvvm.API;

import com.example.tysmaxmvvm.Model.TmdbTopRated_Response;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface TmdbToRated {

    @GET("movie/top_rated")
    Call<TmdbTopRated_Response> gettop_rated_movie(
            @Query("api_key") String api_key
    );

}
