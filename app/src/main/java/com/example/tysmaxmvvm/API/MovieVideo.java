package com.example.tysmaxmvvm.API;

import com.example.tysmaxmvvm.Response.MovieVideoResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MovieVideo {

    @GET("movie/{id}/videos")
    Call<MovieVideoResponse> getmovie_video(
            @Path("id") int movie_id,
            @Query("api_key") String apikey
    );
}
