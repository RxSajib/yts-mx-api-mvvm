package com.example.tysmaxmvvm.API;

import com.example.tysmaxmvvm.Model.TmdbMostpopularResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Tmdb_latest {

    @GET("movie/popular")
    Call<TmdbMostpopularResponse> getmost_popular(
            @Query("api_key") String key,
            @Query("page") int page_no
    );

}
