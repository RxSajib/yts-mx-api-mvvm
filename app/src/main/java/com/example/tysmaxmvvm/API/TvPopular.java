package com.example.tysmaxmvvm.API;

import com.example.tysmaxmvvm.Response.TopRatedTVResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface TvPopular {

    @GET("tv/popular")
    Call<TopRatedTVResponse> get_populartv(
            @Query("api_key") String ApiKey,
            @Query("page") int Page
    );
}
