package com.example.tysmaxmvvm.API;

import com.example.tysmaxmvvm.Response.MovieResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface AllMovieList {

    @GET("list_movies.json")
    Call<MovieResponse> getallmovie(
            @Query("quality") String quary
    );



}
