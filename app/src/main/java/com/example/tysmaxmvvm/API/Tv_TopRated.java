package com.example.tysmaxmvvm.API;

import com.example.tysmaxmvvm.Response.TopRatedTVResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
//toprated_TVShow
public interface Tv_TopRated {

    @GET("tv/top_rated")
    Call<TopRatedTVResponse> toprated_TVShow(
            @Query("api_key") String ApiKey,
            @Query("page") int Page
    );
}
