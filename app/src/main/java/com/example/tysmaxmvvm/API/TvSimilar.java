package com.example.tysmaxmvvm.API;
import com.example.tysmaxmvvm.Response.TvSmilerResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface TvSimilar {

    @GET("tv/{id}/similar")
    Call<TvSmilerResponse> gettv_details(
            @Path("id") int tv_id,
            @Query("api_key") String ApiKey,
            @Query("page") int PageNo
    );
}
