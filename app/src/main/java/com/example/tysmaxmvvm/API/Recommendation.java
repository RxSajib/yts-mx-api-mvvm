package com.example.tysmaxmvvm.API;

import com.example.tysmaxmvvm.Model.Recommendation_Response;
import com.example.tysmaxmvvm.Response.MovieResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Recommendation {

    @GET("movie/{movie_id}/similar")
    Call<Recommendation_Response> getrecommendation_movies(
            @Path("movie_id") int movie_id,
            @Query("api_key") String ApiKey,
            @Query("page") int page_no
    );
}
