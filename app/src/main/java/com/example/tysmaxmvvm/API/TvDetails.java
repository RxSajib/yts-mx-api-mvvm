package com.example.tysmaxmvvm.API;

import com.example.tysmaxmvvm.Model.TvModel;
import com.example.tysmaxmvvm.Response.TvDetailsResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface TvDetails {

    @GET("tv/{id}")
    Call<TvDetailsResponse> tv_details(
            @Path("id") int TvID,
            @Query("api_key") String ApiKey
    );
}
