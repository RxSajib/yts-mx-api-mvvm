package com.example.tysmaxmvvm.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MovieData {

    @SerializedName("movie_count")
    private int movie_count;

    @SerializedName("limit")
    private int limit;

    @SerializedName("page_number")
    private int page_number;

    @SerializedName("movies")
    private List<Movie> movieList;

    public int getMovie_count() {
        return movie_count;
    }

    public void setMovie_count(int movie_count) {
        this.movie_count = movie_count;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getPage_number() {
        return page_number;
    }

    public void setPage_number(int page_number) {
        this.page_number = page_number;
    }

    public List<Movie> getMovieList() {
        return movieList;
    }

    public void setMovieList(List<Movie> movieList) {
        this.movieList = movieList;
    }
}
