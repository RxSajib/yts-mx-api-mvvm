package com.example.tysmaxmvvm.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TmdbMostpopularResponse {

    @SerializedName("total_pages")
    private int total_pages;

    @SerializedName("total_results")
    private int total_results;

    @SerializedName("page")
    private int page;

    @SerializedName("results")
    private List<Tmdb_Movie> tmdb_mostpopulars;

    public int getTotal_pages() {
        return total_pages;
    }

    public void setTotal_pages(int total_pages) {
        this.total_pages = total_pages;
    }

    public int getTotal_results() {
        return total_results;
    }

    public void setTotal_results(int total_results) {
        this.total_results = total_results;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public List<Tmdb_Movie> getTmdb_mostpopulars() {
        return tmdb_mostpopulars;
    }

    public void setTmdb_mostpopulars(List<Tmdb_Movie> tmdb_mostpopulars) {
        this.tmdb_mostpopulars = tmdb_mostpopulars;
    }
}
