package com.example.tysmaxmvvm.Model;

public class MovieBannerModel {

    private int image;

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
