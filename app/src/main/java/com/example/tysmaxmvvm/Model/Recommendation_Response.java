package com.example.tysmaxmvvm.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Recommendation_Response {

    @SerializedName("page")
    private int page;

    @SerializedName("total_pages")
    private int total_pages;

    @SerializedName("total_results")
    private int total_results;

    @SerializedName("results")
    private List<Tmdb_Movie> movieList;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotal_pages() {
        return total_pages;
    }

    public void setTotal_pages(int total_pages) {
        this.total_pages = total_pages;
    }

    public int getTotal_results() {
        return total_results;
    }

    public void setTotal_results(int total_results) {
        this.total_results = total_results;
    }

    public List<Tmdb_Movie> getMovieList() {
        return movieList;
    }

    public void setMovieList(List<Tmdb_Movie> movieList) {
        this.movieList = movieList;
    }
}
