package com.example.tysmaxmvvm.Model;

import com.google.gson.annotations.SerializedName;

public class Season {

    @SerializedName("air_date")
    private String air_date;

    @SerializedName("episode_count")
    private int episode_count;

    @SerializedName("id")
    private int SeasonID;

    @SerializedName("name")
    private String name;

    @SerializedName("overview")
    private String overview;

    @SerializedName("poster_path")
    private String poster_path;


}
