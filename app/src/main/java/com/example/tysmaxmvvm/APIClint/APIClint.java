package com.example.tysmaxmvvm.APIClint;

import com.example.tysmaxmvvm.Data.DataManager;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

public class APIClint {

    private static Retrofit retrofit;

    public Retrofit getRetrofit(){
        if(retrofit == null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(DataManager.BASEURI)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return retrofit;
    }

    public Retrofit gettmdbdata(){
        if(retrofit != null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(DataManager.TMDBBASEURL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

}
