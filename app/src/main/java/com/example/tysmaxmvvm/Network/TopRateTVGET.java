package com.example.tysmaxmvvm.Network;

import android.app.Application;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.tysmaxmvvm.API.Tv_TopRated;
import com.example.tysmaxmvvm.APIClint.APIClint;
import com.example.tysmaxmvvm.Response.TopRatedTVResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TopRateTVGET {

    private Application application;
    private MutableLiveData<TopRatedTVResponse> data;
    private Tv_TopRated tv_topRated;

    public TopRateTVGET(Application application){
        this.application = application;
        data = new MutableLiveData<>();
        tv_topRated = new APIClint().gettmdbdata().create(Tv_TopRated.class);
    }

    public LiveData<TopRatedTVResponse> gettop_tvshows(String ApiKey, int Page){
        tv_topRated.toprated_TVShow(ApiKey, Page)
                .enqueue(new Callback<TopRatedTVResponse>() {
                    @Override
                    public void onResponse(@Nullable Call<TopRatedTVResponse> call,@Nullable Response<TopRatedTVResponse> response) {
                        if(response.isSuccessful()){
                            data.setValue(response.body());
                            Log.d("message", "success");
                        }
                        else {
                            data.setValue(null);
                            Log.d("message", String.valueOf(response.code()));
                        }
                    }

                    @Override
                    public void onFailure(@Nullable Call<TopRatedTVResponse> call, @Nullable Throwable t) {
                        data.setValue(null);
                        Log.d("message", "error");
                    }
                });
        return data;
    }


}
