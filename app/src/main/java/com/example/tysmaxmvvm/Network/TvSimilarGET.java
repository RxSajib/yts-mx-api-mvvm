package com.example.tysmaxmvvm.Network;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.tysmaxmvvm.API.TvSimilar;
import com.example.tysmaxmvvm.APIClint.APIClint;
import com.example.tysmaxmvvm.Response.TvSmilerResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TvSimilarGET {

    private Application application;
    private MutableLiveData<TvSmilerResponse> data;
    private TvSimilar tvSimilar;

    public TvSimilarGET(Application application){
        this.application = application;
        data = new MutableLiveData<>();
        tvSimilar = new APIClint().gettmdbdata().create(TvSimilar.class);
    }

    public LiveData<TvSmilerResponse> getsimiler_movie(int TvID, String ApiKey, int Page){
        tvSimilar.gettv_details(TvID, ApiKey, Page).enqueue(new Callback<TvSmilerResponse>() {
            @Override
            public void onResponse(@Nullable Call<TvSmilerResponse> call,@Nullable Response<TvSmilerResponse> response) {
                if(response.isSuccessful()){
                    data.setValue(response.body());
                }else {
                    Toast.makeText(application, String.valueOf(response.code()), Toast.LENGTH_SHORT).show();
                    data.setValue(null);
                }
            }

            @Override
            public void onFailure(@Nullable Call<TvSmilerResponse> call,@Nullable Throwable t) {
                Toast.makeText(application, t.getMessage(), Toast.LENGTH_SHORT).show();
                data.setValue(null);
            }
        });
        return data;
    }

}
