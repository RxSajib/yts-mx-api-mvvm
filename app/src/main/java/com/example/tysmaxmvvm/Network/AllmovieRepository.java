package com.example.tysmaxmvvm.Network;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.tysmaxmvvm.API.AllMovieList;
import com.example.tysmaxmvvm.APIClint.APIClint;
import com.example.tysmaxmvvm.Response.MovieResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllmovieRepository {

    private Application application;
    private MutableLiveData<MovieResponse> mutableLiveData;
    private AllMovieList allMovieList;

    public AllmovieRepository(Application application){
        this.application = application;
        mutableLiveData = new MutableLiveData<>();
        allMovieList = new APIClint().getRetrofit().create(AllMovieList.class);
    }

    public LiveData<MovieResponse> getallmovies(int page){

        allMovieList.getallmovie("3D").enqueue(new Callback<MovieResponse>() {
            @Override
            public void onResponse(@Nullable Call<MovieResponse> call,@Nullable Response<MovieResponse> response) {
                if(response.isSuccessful()){
                    Toast.makeText(application, "ok", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(application, "error 1", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@Nullable Call<MovieResponse> call,@Nullable Throwable t) {
                Toast.makeText(application, "error 2", Toast.LENGTH_SHORT).show();

            }
        });

        return mutableLiveData;
    }
}
