package com.example.tysmaxmvvm.Network;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.tysmaxmvvm.API.Tmdb_latest;
import com.example.tysmaxmvvm.APIClint.APIClint;
import com.example.tysmaxmvvm.Model.TmdbMostpopularResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TmdbLatestRepository {

    private Application application;
    private MutableLiveData<TmdbMostpopularResponse> data;
    private Tmdb_latest tmdb_latest;

    public TmdbLatestRepository(Application application){
        this.application = application;
        data = new MutableLiveData<>();
        tmdb_latest = new APIClint().gettmdbdata().create(Tmdb_latest.class);
    }

    public LiveData<TmdbMostpopularResponse> getpopularmovies(String apikey, int pageno){
        tmdb_latest.getmost_popular(apikey, pageno).enqueue(new Callback<TmdbMostpopularResponse>() {
            @Override
            public void onResponse(@Nullable Call<TmdbMostpopularResponse> call,@Nullable Response<TmdbMostpopularResponse> response) {
                if(response.isSuccessful()){
                    data.setValue(response.body());
                }
                else {
                    Toast.makeText(application, String.valueOf(response.code()), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@Nullable Call<TmdbMostpopularResponse> call, @Nullable Throwable t) {
                Toast.makeText(application, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        return data;
    }
}
