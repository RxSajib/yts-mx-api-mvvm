package com.example.tysmaxmvvm.Network;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.tysmaxmvvm.API.TmdbTopRatedAllMovie;
import com.example.tysmaxmvvm.APIClint.APIClint;
import com.example.tysmaxmvvm.Model.TmdbTopRated_Response;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TmdbTopRatedAllMoviewRepository {

    private Application application;
    private MutableLiveData<TmdbTopRated_Response> data;
    private TmdbTopRatedAllMovie tmdbTopRatedAllMovie;

    public TmdbTopRatedAllMoviewRepository(Application application){
        this.application = application;
        data = new MutableLiveData<>();
        tmdbTopRatedAllMovie = new APIClint().gettmdbdata().create(TmdbTopRatedAllMovie.class);
    }

    public LiveData<TmdbTopRated_Response> getalltopratedmovies(String Apikey, int Page){
        tmdbTopRatedAllMovie.getalltoprated_movie(Apikey, Page)
                .enqueue(new Callback<TmdbTopRated_Response>() {
                    @Override
                    public void onResponse(@Nullable Call<TmdbTopRated_Response> call,@Nullable Response<TmdbTopRated_Response> response) {
                        if(response.isSuccessful()){
                            data.setValue(response.body());
                        }else {
                            data.setValue(null);
                            Toast.makeText(application, String.valueOf(response.code()), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(@Nullable Call<TmdbTopRated_Response> call,@Nullable Throwable t) {
                        data.setValue(null);
                        Toast.makeText(application, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
        return data;
    }
}
