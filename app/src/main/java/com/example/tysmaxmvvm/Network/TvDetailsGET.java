package com.example.tysmaxmvvm.Network;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.tysmaxmvvm.API.TvDetails;
import com.example.tysmaxmvvm.APIClint.APIClint;
import com.example.tysmaxmvvm.Model.TvModel;
import com.example.tysmaxmvvm.Response.TvDetailsResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TvDetailsGET {

    private Application application;
    private MutableLiveData<TvDetailsResponse> data;
    private TvDetails tvDetails;

    public TvDetailsGET(Application application){
        this.application = application;
        data = new MutableLiveData<>();
        tvDetails = new APIClint().gettmdbdata().create(TvDetails.class);
    }

    public LiveData<TvDetailsResponse> gettvdetails(int TvID, String ApiKey){
        tvDetails.tv_details(TvID, ApiKey).enqueue(new Callback<TvDetailsResponse>() {
            @Override
            public void onResponse(@Nullable Call<TvDetailsResponse> call,@Nullable Response<TvDetailsResponse> response) {
                if(response.isSuccessful()){
                    data.setValue(response.body());
                }else {
                    Toast.makeText(application, String.valueOf(response.code()), Toast.LENGTH_LONG).show();
                    data.setValue(null);
                }
            }

            @Override
            public void onFailure(@Nullable Call<TvDetailsResponse> call,@Nullable Throwable t) {
                Toast.makeText(application, t.getMessage(), Toast.LENGTH_SHORT).show();
                data.setValue(null);
            }
        });
        return data;
    }
}
