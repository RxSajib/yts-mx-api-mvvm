package com.example.tysmaxmvvm.Network;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.tysmaxmvvm.API.TmdbToRated;
import com.example.tysmaxmvvm.APIClint.APIClint;
import com.example.tysmaxmvvm.Model.TmdbTopRated_Response;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TmdbTopRated_Repository {

    private Application application;
    private MutableLiveData<TmdbTopRated_Response> data;
    private TmdbToRated tmdbToRated;

    public TmdbTopRated_Repository(Application application){
        this.application = application;
        data = new MutableLiveData<>();
        tmdbToRated = new APIClint().gettmdbdata().create(TmdbToRated.class);
    }

    public LiveData<TmdbTopRated_Response> get_top_ratedmovies(String ApiKey){
        tmdbToRated.gettop_rated_movie(ApiKey).enqueue(new Callback<TmdbTopRated_Response>() {
            @Override
            public void onResponse(@Nullable Call<TmdbTopRated_Response> call,@Nullable Response<TmdbTopRated_Response> response) {
                if(response.isSuccessful()){
                    data.setValue(response.body());
                }else {
                    Toast.makeText(application, String.valueOf(response.code()), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@Nullable Call<TmdbTopRated_Response> call,@Nullable Throwable t) {
                Toast.makeText(application, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        return data;
    }

}
