package com.example.tysmaxmvvm.Network;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.tysmaxmvvm.API.MovieVideo;
import com.example.tysmaxmvvm.APIClint.APIClint;
import com.example.tysmaxmvvm.Response.MovieVideoResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieVideoGET {

    private Application application;
    private MutableLiveData<MovieVideoResponse> data;
    private MovieVideo movieVideo;

    public MovieVideoGET(Application application){
        this.application = application;
        data = new MutableLiveData<>();
        movieVideo = new APIClint().gettmdbdata().create(MovieVideo.class);
    }

    public LiveData<MovieVideoResponse> getmovievideo(int MovieID, String ApiKey){
        movieVideo.getmovie_video(MovieID, ApiKey).enqueue(new Callback<MovieVideoResponse>() {
            @Override
            public void onResponse(@Nullable Call<MovieVideoResponse> call,@Nullable Response<MovieVideoResponse> response) {
                if(response.isSuccessful()){
                    data.setValue(response.body());
                }else {
                    Toast.makeText(application, String.valueOf(response.code()), Toast.LENGTH_SHORT).show();
                    data.setValue(null);
                }
            }

            @Override
            public void onFailure(@Nullable Call<MovieVideoResponse> call,@Nullable Throwable t) {
                Toast.makeText(application, t.getMessage(), Toast.LENGTH_SHORT).show();
                data.setValue(null);
            }
        });
        return data;
    }
}
