package com.example.tysmaxmvvm.Network;

import android.app.Application;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.tysmaxmvvm.API.TvPopular;
import com.example.tysmaxmvvm.APIClint.APIClint;
import com.example.tysmaxmvvm.Response.TopRatedTVResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TVPopularGET {

    private Application application;
    private MutableLiveData<TopRatedTVResponse> data;
    private TvPopular tvPopular;

    public TVPopularGET(Application application){
        this.application = application;
        data = new MutableLiveData<>();
        tvPopular = new APIClint().gettmdbdata().create(TvPopular.class);
    }

    public LiveData<TopRatedTVResponse> getToprated_TV(String Apikey, int Page){
        tvPopular.get_populartv(Apikey, Page)
                .enqueue(new Callback<TopRatedTVResponse>() {
                    @Override
                    public void onResponse(@Nullable Call<TopRatedTVResponse> call,@Nullable Response<TopRatedTVResponse> response) {
                        if(response.isSuccessful()){
                            data.setValue(response.body());
                            Log.d("response", "ok");
                        }else {
                            Log.d("response", "code");
                            data.setValue(null);
                        }
                    }

                    @Override
                    public void onFailure(@Nullable Call<TopRatedTVResponse> call,@Nullable Throwable t) {
                        Log.d("response", t.getMessage());
                        data.setValue(null);
                    }
                });
        return data;
    }
}
