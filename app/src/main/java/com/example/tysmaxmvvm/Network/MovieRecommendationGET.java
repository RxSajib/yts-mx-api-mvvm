package com.example.tysmaxmvvm.Network;

import android.app.Application;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.tysmaxmvvm.API.Recommendation;
import com.example.tysmaxmvvm.APIClint.APIClint;
import com.example.tysmaxmvvm.Model.Recommendation_Response;
import com.example.tysmaxmvvm.Response.MovieResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieRecommendationGET {

    private Application application;
    private MutableLiveData<Recommendation_Response> data;
    private Recommendation recommendation;

    public MovieRecommendationGET(Application application){
        this.application = application;
        data = new MutableLiveData<>();
        recommendation = new APIClint().gettmdbdata().create(Recommendation.class);
    }

    public LiveData<Recommendation_Response> getrecommedtion_movie(int movie_id, String ApiKey, int page_no){
        recommendation.getrecommendation_movies(movie_id, ApiKey, page_no).enqueue(new Callback<Recommendation_Response>() {
            @Override
            public void onResponse(@Nullable Call<Recommendation_Response> call,@Nullable Response<Recommendation_Response> response) {
                if(response.isSuccessful()){
                    data.setValue(response.body());
                }else {
                    data.setValue(null);
                    Toast.makeText(application, String.valueOf(response.code()), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@Nullable Call<Recommendation_Response> call,@Nullable Throwable t) {
                data.setValue(null);
                Toast.makeText(application, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        return data;
    }
}
