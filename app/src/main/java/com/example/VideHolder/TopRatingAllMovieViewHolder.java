package com.example.VideHolder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tysmaxmvvm.Model.Movie;
import com.example.tysmaxmvvm.R;

import org.jetbrains.annotations.NotNull;
import org.w3c.dom.Text;

public class TopRatingAllMovieViewHolder extends RecyclerView.ViewHolder {

    public ImageView imageView;
    public TextView MovieTitle, MoviePopularty, Rating;

    public TopRatingAllMovieViewHolder(@NonNull @NotNull View itemView) {
        super(itemView);

        imageView = itemView.findViewById(R.id.ImageViewID);
        MovieTitle = itemView.findViewById(R.id.MovieTitleID);
        MoviePopularty = itemView.findViewById(R.id.PopulartyText);
        Rating = itemView.findViewById(R.id.RatingTextID);
    }
}
