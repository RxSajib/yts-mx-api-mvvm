package com.example.VideHolder;

import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tysmaxmvvm.R;
import com.makeramen.roundedimageview.RoundedImageView;

import org.jetbrains.annotations.NotNull;

public class MovieBannerViewHolder extends RecyclerView.ViewHolder {

    public RoundedImageView moviebannerimage;

    public MovieBannerViewHolder(@NonNull @NotNull View itemView) {
        super(itemView);

        moviebannerimage = itemView.findViewById(R.id.MovieBannerImage);
    }
}
