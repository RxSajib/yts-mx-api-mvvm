package com.example.VideHolder;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tysmaxmvvm.R;
import com.makeramen.roundedimageview.RoundedImageView;

import org.jetbrains.annotations.NotNull;

public class Tmdb_Mostpopular_movieViewHolder extends RecyclerView.ViewHolder {

    public RoundedImageView imageView;

    public Tmdb_Mostpopular_movieViewHolder(@NonNull @NotNull View itemView) {
        super(itemView);

        imageView = itemView.findViewById(R.id.PopularMovieImage);
    }
}
