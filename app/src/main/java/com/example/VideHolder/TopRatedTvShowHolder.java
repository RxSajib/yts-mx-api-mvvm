package com.example.VideHolder;

import android.media.Image;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tysmaxmvvm.R;

import org.jetbrains.annotations.NotNull;

public class TopRatedTvShowHolder extends RecyclerView.ViewHolder {

    public ImageView imageView;
    public TextView Title, Popularty, Rating;

    public TopRatedTvShowHolder(@NonNull @NotNull View itemView) {
        super(itemView);

        imageView = itemView.findViewById(R.id.ImageViewID);
        Title = itemView.findViewById(R.id.MovieTitleID);
        Popularty = itemView.findViewById(R.id.PopulartyText);
        Rating = itemView.findViewById(R.id.RatingTextID);
    }
}
