package com.example.VideHolder;

import android.media.Image;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tysmaxmvvm.R;

public class OnBoardingViewHolder extends RecyclerView.ViewHolder {

    public ImageView imageView;

    public OnBoardingViewHolder(@NonNull @org.jetbrains.annotations.NotNull View itemView) {
        super(itemView);

        imageView = itemView.findViewById(R.id.onboardingImage);
    }
}
