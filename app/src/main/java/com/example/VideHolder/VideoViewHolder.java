package com.example.VideHolder;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tysmaxmvvm.R;

import org.jetbrains.annotations.NotNull;

public class VideoViewHolder extends RecyclerView.ViewHolder {

    public TextView name, site, size;

    public VideoViewHolder(@NonNull @NotNull View itemView) {
        super(itemView);

        name = itemView.findViewById(R.id.Name);
        site = itemView.findViewById(R.id.SiteName);
        size = itemView.findViewById(R.id.Size);
    }
}
