package com.example.VideHolder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.tysmaxmvvm.R;

import org.jetbrains.annotations.NotNull;

public class RecommendationViewHolder extends RecyclerView.ViewHolder {

    public ImageView ImageView;
    public TextView Title, Popularty, Rating;

    public RecommendationViewHolder(@NonNull @NotNull View itemView) {
        super(itemView);

        ImageView = itemView.findViewById(R.id.ImageViewID);
        Title = itemView.findViewById(R.id.TitleText);
        Popularty = itemView.findViewById(R.id.PopulartyText);
        Rating = itemView.findViewById(R.id.RatingTextID);
    }
}
