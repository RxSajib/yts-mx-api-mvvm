package com.example.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.tysmaxmvvm.API.TvSimilar;
import com.example.tysmaxmvvm.Network.TVPopularGET;
import com.example.tysmaxmvvm.Network.TopRateTVGET;
import com.example.tysmaxmvvm.Network.TvDetailsGET;
import com.example.tysmaxmvvm.Network.TvSimilarGET;
import com.example.tysmaxmvvm.Response.TopRatedTVResponse;
import com.example.tysmaxmvvm.Response.TvDetailsResponse;
import com.example.tysmaxmvvm.Response.TvSmilerResponse;

import org.jetbrains.annotations.NotNull;

public class TvShowsViewModel extends AndroidViewModel {

    private TopRateTVGET topRateTVGET;
    private TVPopularGET tvPopularGET;
    private TvDetailsGET tvDetailsGET;
    private TvSimilarGET similarGET;


    public TvShowsViewModel(@NonNull @NotNull Application application) {
        super(application);

        topRateTVGET = new TopRateTVGET(application);
        tvPopularGET = new TVPopularGET(application);
        tvDetailsGET = new TvDetailsGET(application);
        similarGET = new TvSimilarGET(application);
    }

    public LiveData<TopRatedTVResponse> gettop_ratedTv(String Apikey, int Page){
        return topRateTVGET.gettop_tvshows(Apikey, Page);
    }

    public LiveData<TopRatedTVResponse> tvpopular(String Apikey, int Page){
      return   tvPopularGET.getToprated_TV(Apikey, Page);
    }

    public LiveData<TvDetailsResponse> gettv_details(int TvID, String ApiKey){
        return tvDetailsGET.gettvdetails(TvID, ApiKey);
    }

    public LiveData<TvSmilerResponse> get_tvsimiler(int TvID, String ApiKey, int Page){
        return similarGET.getsimiler_movie(TvID, ApiKey, Page);
    }
}
