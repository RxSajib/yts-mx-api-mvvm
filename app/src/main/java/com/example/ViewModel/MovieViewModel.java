package com.example.ViewModel;

import android.app.Application;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.tysmaxmvvm.API.TmdbTopRatedAllMovie;
import com.example.tysmaxmvvm.Model.Recommendation_Response;
import com.example.tysmaxmvvm.Model.TmdbMostpopularResponse;
import com.example.tysmaxmvvm.Model.TmdbTopRatedMovies;
import com.example.tysmaxmvvm.Model.TmdbTopRated_Response;
import com.example.tysmaxmvvm.Network.MovieRecommendationGET;
import com.example.tysmaxmvvm.Network.MovieVideoGET;
import com.example.tysmaxmvvm.Network.TmdbLatestRepository;
import com.example.tysmaxmvvm.Network.TmdbTopRatedAllMoviewRepository;
import com.example.tysmaxmvvm.Network.TmdbTopRated_Repository;
import com.example.tysmaxmvvm.Response.MovieResponse;
import com.example.tysmaxmvvm.Network.AllmovieRepository;
import com.example.tysmaxmvvm.Response.MovieVideoResponse;
import com.example.tysmaxmvvm.Room.Database;
import com.example.tysmaxmvvm.Room.TopRatedMovieDio;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class MovieViewModel extends AndroidViewModel {

    private AllmovieRepository allmovieRepository;
    private TmdbLatestRepository tmdbLatestRepository;
    private TmdbTopRated_Repository tmdbTopRated_repository;
    private TmdbTopRatedAllMoviewRepository tmdbTopRatedAllMoviewRepository;
    private MovieRecommendationGET movieRecommendationGET;
    private MovieVideoGET videoGET;

    private Database database;
    private TopRatedMovieDio topRatedMovieDio;

    public MovieViewModel(@NonNull @NotNull Application application) {
        super(application);

        allmovieRepository = new AllmovieRepository(application);
        tmdbLatestRepository = new TmdbLatestRepository(application);
        tmdbTopRated_repository = new TmdbTopRated_Repository(application);
        tmdbTopRatedAllMoviewRepository = new TmdbTopRatedAllMoviewRepository(application);
        movieRecommendationGET = new MovieRecommendationGET(application);
        videoGET = new MovieVideoGET(application);

        database = Database.getDatabase(application);
        topRatedMovieDio = database.topRatedMovieDio();
    }

    public LiveData<MovieResponse> getallmovies(){
        return allmovieRepository.getallmovies(1);

    }

    public LiveData<TmdbMostpopularResponse> gettmbd_latestmovies(String Apikey, int pageno){
        return tmdbLatestRepository.getpopularmovies(Apikey, pageno);
    }

    public LiveData<TmdbTopRated_Response> gettoprated_movies(String Apikey){
        return tmdbTopRated_repository.get_top_ratedmovies(Apikey);
    }

    public LiveData<TmdbTopRated_Response> getalltopratedmovies(String ApiKey, int page){
        return tmdbTopRatedAllMoviewRepository.getalltopratedmovies(ApiKey, page);
    }

    public LiveData<Recommendation_Response> getrecomandation_movie(int movie_id, String ApiKey, int page_no){
        return movieRecommendationGET.getrecommedtion_movie(movie_id, ApiKey, page_no);
    }

    public LiveData<MovieVideoResponse> getmovie_video(int MovieID, String ApiKey){
        return videoGET.getmovievideo(MovieID, ApiKey);
    }

    //todo insert getall room toprated movie ----------------

    public LiveData<List<TmdbTopRatedMovies>> get_topratedmovielist(){
        return topRatedMovieDio.getallTopratedmovie();
    }

    public void insert_topratedmovie(List<TmdbTopRatedMovies> list){
        new InsertTopRatedMovieAsyTask(topRatedMovieDio).execute(list);
    }

    class InsertTopRatedMovieAsyTask extends AsyncTask<List<TmdbTopRatedMovies>, Void, Void>{

        private TopRatedMovieDio topRatedMovieDio;

        public InsertTopRatedMovieAsyTask(TopRatedMovieDio topRatedMovieDio) {
            this.topRatedMovieDio = topRatedMovieDio;
        }

        @Override
        protected Void doInBackground(List<TmdbTopRatedMovies>... lists) {
            topRatedMovieDio.inserttopratedmovie(lists[0]);
            return null;
        }
    }
    //todo insert getall room toprated movie ----------------
 }
